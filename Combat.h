#ifndef ISITECH_CPP_COMBAT_H
#define ISITECH_CPP_COMBAT_H
#include <iostream>
#include "Boxeur.h"
using namespace std;

namespace box{
    class Combat {
    string niveau;
    Boxeur *coinBleu;
    Boxeur *coinRouge;
    Boxeur *vainqueur;
    public:
        Combat(string niveau);
        ~Combat();
        string GetNiveau();
        Boxeur* GetCoinBleu();
        Boxeur* GetCoinRouge();
        void SetCoinBleu(Boxeur *boxeur);
        void SetCoinRouge(Boxeur *boxeur);
        Boxeur* DesignerVainqeur(string coinGagnant);
        Boxeur* GetVainqueur();
    };
}

#endif //ISITECH_CPP_COMBAT_H
