#include <iostream>
#include "Boxeur.h"
#include "Combat.h"

using namespace box;

void AfficherResultat(Combat combat[3]) {
    for (int i = 0; i < 3; ++i) {
        cout << "Combat " << combat[i].GetNiveau() << endl;
        cout << " \t Coin bleu: " << combat[i].GetCoinBleu()->GetName() << endl;
        cout << "\t Coin rouge: " << combat[i].GetCoinRouge()->GetName() << endl;
        cout << "\t Vainqueur: " << combat[i].GetVainqueur()->GetName() << endl;
    }
}


int main() {
    Boxeur boxeur_1("Box_1", 75);
    Boxeur boxeur_2("Box_2", 78);
    Boxeur boxeur_3("Box_3", 80);
    Boxeur boxeur_4("Box_4", 75);

    Combat *competition[3] = {new Combat("1/4"), new Combat("1/4"), new Combat("1/2")};

    competition[0]->SetCoinBleu(&boxeur_1);
    competition[0]->SetCoinRouge(&boxeur_2);
    Boxeur *gagant_1 = competition[0]->DesignerVainqeur("bleu");

    competition[1]->SetCoinBleu(&boxeur_3);
    competition[1]->SetCoinRouge(&boxeur_4);
    Boxeur *gagant_2 = competition[1]->DesignerVainqeur("rouge");

    competition[2]->SetCoinBleu(gagant_1);
    competition[2]->SetCoinRouge(gagant_2);
    competition[2]->DesignerVainqeur("rouge");

    AfficherResultat(*competition);

    return 0;
}
