#include "Combat.h"

using namespace box;

Combat::Combat(string niveau) {
    this->niveau = niveau;
}

string Combat::GetNiveau() {
    return this->niveau;
}

Boxeur *Combat::GetCoinBleu() {
    return this->coinBleu;
}

Boxeur *Combat::GetCoinRouge() {
    return this->coinRouge;
}

void Combat::SetCoinBleu(Boxeur *boxeur) {
    if (boxeur == this->coinRouge) {
        cout << "Boxeur bleu ne doit pas être le même que le boxeur rouge" << endl;
        return;
    }

    this->coinBleu = boxeur;
}

void Combat::SetCoinRouge(Boxeur *boxeur) {
    if (boxeur == this->coinBleu) {
        cout << "Boxeur rouge ne doit pas être le même que le boxeur bleu" << endl;
        return;
    }

    this->coinRouge = boxeur;
}

Boxeur* Combat::DesignerVainqeur(string coinGagnant) {
    if (coinGagnant == "bleu") {
        this->vainqueur = this->coinBleu;
    } else if (coinGagnant == "rouge") {
        this->vainqueur = this->coinRouge;
    }else{
        this->vainqueur = nullptr;
    }

    return this->vainqueur;
}

Boxeur* Combat::GetVainqueur() {
    return this->vainqueur;
}

Combat::~Combat() {
}
