#include "Boxeur.h"

using namespace box;

Boxeur::Boxeur(string name, double poid) : name(name), poid(poid) {}

string Boxeur::GetName() {
    return this->name;
}

void Boxeur::SetPoid(double poid) {
    this->poid = poid;
}

double Boxeur::GetPoid() {
    return this->poid;
}

Boxeur::Boxeur() {

}

Boxeur::~Boxeur() {

}
