#ifndef ISITECH_CPP_BOXEUR_H
#define ISITECH_CPP_BOXEUR_H

#include <iostream>

using namespace std;

namespace box {
    class Boxeur {
        string name;
        double poid = 0.0;

    public:
        Boxeur();
        ~Boxeur();

        Boxeur(string name, double poid);

        string GetName();

        void SetPoid(double poid);

        double GetPoid();

    };
}

#endif